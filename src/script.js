//================================= Capture de mes ID et classes =================================//


//Section Start
let start = document.querySelector("#start");

//Section Guide
let level = document.querySelector("#level");
let consigne = document.querySelector("#consigne");
let exit = document.querySelector("#exit");
let continueBtn = document.querySelector("#continue");
let suiteBtn = document.querySelector("#suite");

//Section Quiz
let quiz = document.querySelector("#quiz");
let time = document.querySelector("#time");

//Section Questions
let questionNo = document.querySelector("#questionNo");
let questionText = document.querySelector("#questionText");
let myImage = document.querySelector('.image')

//Réponses multiples
let option1 = document.querySelector("#option1");
let option2 = document.querySelector("#option2");
let option3 = document.querySelector("#option3");
let option4 = document.querySelector("#option4");

//Point et Button next
let total_point = document.querySelector("#total_point");
let next_question = document.querySelector("#next_question");

//Section Resultat
let result = document.querySelector("#result");
let points = document.querySelector("#points");
let quit = document.querySelector("#quit");
let startAgain = document.querySelector("#startAgain");

//Tous les 'H4' de la Section Quiz (MYQUIZ)
let differentchoices = document.querySelectorAll(".differentchoices");


//================================= Déclaration de mon Objet =================================//


let myQuiz = [{
    myQuestion: "Qu'est-ce que Luke Skywalker a perdu dans son combat avec Dark Vador?",
    choice1: "Sa main gauche",
    choice2: "Son pied gauche",
    choice3: "Sa main droite",
    choice4: "Sa jambe gauche",
    answer: 2
},
{
    myQuestion: "Où la guerre des clones a-t-elle commencé?",
    choice1: "Tatooine",
    choice2: "Géonose",
    choice3: "Naboo",
    choice4: "Coruscant",
    answer: 1
},
{
    myQuestion: "Quel surnom Han Solo donne à Luke Skywalker?",
    choice1: "Buckaroo",
    choice2: "Enfant",
    choice3: "Skydancer",
    choice4: "Lukie",
    answer: 1
},
{
    myQuestion: "Qui a fait exploser la première étoile de la mort et avec quelle arme?",
    choice1: "Luke Skywalker avec son sabre laser",
    choice2: "Princesse Leia avec un X-Wing",
    choice3: "Luke Skywalker avec un X-Wing",
    choice4: "Princesse Leia avec un détonateur thermique",
    answer: 2
},
{
    myQuestion: " Quel était le titre original du film Star Wars?",
    choice1: "Batailles d'étoiles",
    choice2: "Aventures de Luke Starkiller",
    choice3: "Les aventures des Jedi",
    choice4: "Batailles dans l'espace",
    answer: 1
},
{
    myQuestion: "Quelle est l'arme de choix de Chewbacca?",
    choice1: "Fusil blaster",
    choice2: "Sabre laser",
    choice3: "Club de métal",
    choice4: "Bowcaster",
    answer: 3
},
{
    myQuestion: "Selon l'empereur, quelle était la faiblesse de Luke Skywalker?",
    choice1: "Sa foi dans le côté léger de la force",
    choice2: "Sa foi en ses amis",
    choice3: "Son manque de vision",
    choice4: "Sa résistance au côté obscur de la force",
    answer: 1
}, {
    myQuestion: "Combien mesure Chewbacca ?",
    choice1: "1,80 m",
    choice2: "2,28 m",
    choice3: "2,35 m",
    choice4: "3,25 m",
    answer: 1
},
{
    myQuestion: "De quelle couleur est le sabre laser d'Obi-Wan Kenobi ?",
    choice1: "jaune",
    choice2: "vert",
    choice3: "rouge",
    choice4: "bleu",
    answer: 3
},
{
    myQuestion: "De quelle planète est originaire Jar Jar Binks?",
    choice1: "Alderaan",
    choice2: "Naboo",
    choice3: "Corellia",
    choice4: "Utapeau",
    answer: 1
}];

//================================= Déclaration des Variables =================================//



let index = 0;
let timer = 15;
let interval = 0;

//total points
let correct = 0;



//Tableau de mes images

let imageArray = [
'images/bg0.jpg',
'images/bg1.jpg', 
'images/bg2.jpg',
'images/bg3.jpg',
'images/bg4.jpg',
'images/bg5.jpg',
'images/bg6.jpg', 
'images/bg7.jpg',
'images/bg8.jpg',
'images/bg9.jpg',
'images/bg10.jpg',
]



let imageIndex = 0;

//========================================= Events  =============================================//

//Evenement clic sur le Button 'Start'
start.addEventListener("click", () => {
    start.style.display = "none";
    consigne.style.display = "block";
});


//Evenement clic sur le Button 'Exit'
exit.addEventListener("click", () => {
    start.style.display = "block";
    consigne.style.display = "none";
});



// Button continue 

continueBtn.addEventListener("click", () => {
    quiz.style.display = "block";
    consigne.style.display = "none";
    myImage.src = imageArray[imageIndex];
    

    interval = setInterval(countDown, 1000);
    loadData();

    removeActiveClass(differentchoices);

    total_point.style.display = "block";
    total_point.textContent = `Actuellement vous avez ${correct = 0} points sur  ${myQuiz.length} questions`;
});



////Bouton suite

next_question.addEventListener("click", () => {
    //    if index  est inférieur à myQuiz.length
    if (index < myQuiz.length - 1) {
        index++;
        imageIndex++;


       myImage.src = imageArray[imageIndex];
    

        removeActiveClass(differentchoices)

        //Appel de ma fonction loadData
        loadData();

        //resultat
        total_point.style.display = "block";
        total_point.textContent = `Actuellement vous avez ${correct} points sur ${myQuiz.length} questions`;
        clearInterval(interval);
        interval = setInterval(countDown, 1000);
    } else {
        index = 0;
        imageIndex = 0;


        //Apparaition section Resultat en fin de Quiz
        clearInterval(interval);
        quiz.style.display = "none";
        points.textContent = `Resultat : ${correct} points sur ${myQuiz.length}`;
        result.style.display = "block";
    }

    
})

//Event sur le Button Quittez
quit.addEventListener("click", () => {
    start.style.display = "block";
    result.style.display = "none";
    consigne.style.display = "none";

});

//Event sur le Button Recommencez
startAgain.addEventListener("click", () => {
    consigne.style.display = "block";
    result.style.display = "none";
});



//verification reponse et Calcul des points

differentchoices.forEach((choices, choiceNo) => {
    choices.addEventListener("click", () => {
        choices.classList.add("active");
        //checker la bonne réponse
        if (choiceNo === myQuiz[index].answer) {
            correct++;
            choices.classList.add('choice_true');


        } else {
            correct += 0;
            choices.classList.add('choice_wrong');
        }
        //stop Compteur
        clearInterval(interval);

        //Ajout de ma classe desactive_clic

        for (const item of differentchoices) {
            item.classList.add("desactive_clic");
        }

    })
});


//======================================= Fonction =========================================//

//  apparaitre mes objects MYQUIZ

let loadData = () => {
    questionNo.textContent = index + 1 + ". ";
    questionText.textContent = myQuiz[index].myQuestion;
    option1.textContent = myQuiz[index].choice1;
    option2.textContent = myQuiz[index].choice2;
    option3.textContent = myQuiz[index].choice3;
    option4.textContent = myQuiz[index].choice4;

    timer = 15;
   
}

//  Enlever toutes les classes  quand le bouton Continue est cliquer

function removeActiveClass(paramClass) {
    for (const items of paramClass) {
        items.classList.remove('active');
        items.classList.remove('desactive_clic');
        items.classList.remove('choice_wrong');
        items.classList.remove('choice_true');
    }
}



//Creation du Timer Section Quiz

let countDown = () => {
    if (timer === 0) {
        clearInterval(interval);
        next_question.click();
    } else {
        timer--;
        time.textContent = timer;
    }
}

