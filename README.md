
<h1>PROJET QUIZ</h1>
<h3>Dans le cadre de ma formation de développeur Web chez Simplon, je dois réaliser un quiz</h3>

- PROJET : [Quiz](https://kamel-ben-abdelkader.gitlab.io/projet-quiz)

Processus du Projet : J'ai d'abord recherché le sujet de mon quiz, puis j'ai réalisé une maquette, ensuite j'ai réfélchis par écrit aux principales fonctionnalités que je devais créer (afficher mes datas, les classes à ajouter et à enlever, les block à apparaitre ou disparaître , l'incrémentation de l'index de mon tableau de mes objets, l'ajout de photos, la prise en compte du score...)
Ensuite j'ai realisé mon fichier css, ainsi que mon fichier script.js fonctionnalité par fonctionalité.

-----------------

<h3>Maquette réalisée</h3>

<img src="public/images/maquette/start-quiz.jpg/" width="300" height="300" />
<img src="public/images/maquette/consigne-quiz.jpg" width="300" height="300" />
<img src="public/images/maquette/qcm-quiz.jpg" width="300" height="300" />
<img src="public/images/maquette/final-quiz.jpg" width="300" height="300" />




-----------------

- 👨‍💻 Tous mes projets en cours sont disponibles sur mon GITLAB : [https://gitlab.com/Kamel.benabdelkader](https://gitlab.com/Kamel.benabdelkader)


-----------------

<h3>Me contacter:</h3>
<p align="left">
<a href="https://linkedin.com/in/https://www.linkedin.com/in/kamel-ben-abdelkader-094928167/" target="blank"><img align="center" src="public/images/maquette/linkedin.png" alt="https://www.linkedin.com/in/kamel-ben-abdelkader-094928167/" height="30" width="40" /></a>
</p>

-----------------

<h3>Language et outil:</h3>
- HTML
- CSS
- JAVASCRIPT